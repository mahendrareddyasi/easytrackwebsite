﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyTrackWebsite.Models
{
    public class FlightMode
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}