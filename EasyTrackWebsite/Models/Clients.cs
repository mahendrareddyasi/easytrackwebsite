﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyTrackWebsite.Models
{
    public class Clients
    {
        public int ID { get; set; }
        public string ClientName { get; set; }
        public string ClientPhoneNo { get; set; }
        public string InsertedUser { get; set; }
        public System.DateTime InsertedDate { get; set; }
        public string UpdatedUser { get; set; }
        public System.DateTime UpdatedDate { get; set; }

    }
}
